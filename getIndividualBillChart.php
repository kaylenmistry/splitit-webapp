<?php 
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $billID = $_POST["billID"];

    $selectQuery = $db->prepare("SELECT * FROM userBills WHERE userID=:userID AND billID=:billID");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $selectQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    if ($row = $result->fetchArray()) {
        $amountToPay = $row['amountToPay'];
        $amountPaid = $row['amountPaid'];
        echo json_encode(array("amountToPay" => $amountToPay, "amountPaid" => $amountPaid));
    }

?>