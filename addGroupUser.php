<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();
    $username = $_POST["username"];
    $users = $_POST["users"];
    $groupID = $_SESSION["groupID"];
    $authorUsername = $_SESSION["username"];

    foreach ($users as $user) {
        if ($user === $username) { return; }
    }

    $selectQuery = $db->prepare("SELECT userID FROM users WHERE username=:username LIMIT 1");
    $selectQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $result = $selectQuery->execute();
    $userID = $result->fetchArray();
    $userID = $userID['userID'];

    if (!$userID) { return; }

    $insertQuery = $db->prepare("INSERT INTO userGroups VALUES (:userID, :groupID, 0)");
    $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $insertQuery->execute();

    $insertQuery = $db->prepare("INSERT INTO groupRequests VALUES (:userID, :groupID, :authorUsername)");
    $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':authorUsername', $authorUsername, SQLITE3_TEXT);
    $result = $insertQuery->execute();
    
    echo $userID;
?>
