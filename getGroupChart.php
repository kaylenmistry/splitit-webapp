<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $groupID = $_SESSION["groupID"];

    $selectQuery = $db->prepare("SELECT * FROM users INNER JOIN userBills ON users.userID=userBills.userID INNER JOIN bills ON userBills.billID = bills.billID WHERE groupID=:groupID ORDER BY users.userID");
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    $int = 0;
    $data = array();
    $currentUsername;
    $amountPaid = 0;
    $amountPending = 0;
    while ($row = $result->fetchArray()) {
        if (!isset($currentUsername)) {
            $currentUsername = $row["username"];
        } else if ($currentUsername !== $row["username"]) {
            $data[$currentUsername] = array($amountPaid, $amountPending);
            $currentUsername = $row["username"];
            $amountPaid = 0;
            $amountPending = 0;
        }
        $amountPaid += $row['amountPaid'];
        $amountPending += ($row['amountToPay'] - $row['amountPaid']);
        $int += 1;
    }
    if (isset($currentUsername)) {
        $data[$currentUsername] = array($amountPaid, $amountPending);
    }
    echo json_encode($data);

?>
