$(document).ready(function() {
  var modal = $('#modal');
  var detailIsHidden = true;

  $('#loginButton').on('click', function() {
    $('#modalTitle').html("welcome back");
    $('#forgotPassword').show();
    $('.registerInput').hide();
    $('input[type="submit"]').val("log in");
    $('#errorMessage').html("");
    openModal();
  });

  $('#registerButton').on('click', function() {
    $('#modalTitle').html("register now");
    $('#forgotPassword').hide();
    $('.registerInput').show();
    $('input[type="submit"]').val("register");
    $('#errorMessage').html("");
    openModal();
  });

  $('#modal').on('click', function(event) {
    if ($(event.target).attr('class') !== 'modalContent' && !$(event.target).parents('.modalContent').length) {
      closeModal(function() {
        resetModal();
      });
    }
  });

  $('#webappLogin').on('click', function(event) {
    event.preventDefault();
    var username = $('input[name=username]').val().toLowerCase();
    var password = $('input[name=password]').val();
    if ($('input[type="submit"]').val() === "register") {
      var firstname = $('input[name=firstname]').val();
      var lastname = $('input[name=lastname]').val();
      var email = $('input[name=email]').val();
      var confirmPassword = $('input[name=confirmPassword]').val();

      $.ajax({
        url: "register.php",
        type: "POST",
        data: {
          firstname: firstname,
          lastname: lastname,
          email: email,
          username: username,
          password: password,
          confirmPassword: confirmPassword
        },
        success: function(data) {
          if (data) {
            $('#errorMessage').html("<strong>" + data.toLowerCase() + "</strong>");
            shakeMessage();
          } else {
            window.location.replace('index.php');
          }
        }
      });
    } else {
      $.ajax({
        url: "login.php",
        type: "POST",
        data: {
          username: username,
          password: password
        },
        success: function(data) {
          if (data) {
            $('#errorMessage').html("<strong>" + data.toLowerCase() + "</strong>");
            shakeMessage();
          } else {
            window.location.replace('index.php');
          }
        }
      });
    }
  });

  function shakeMessage() {
    $('#errorMessage').css('animation-name', 'shake');
    setTimeout(function() {
      $('#errorMessage').css('animation-name', 'none');
    }, 900);
  }

  $('#logoutButton').on('click', function() {
    window.location.replace('logout.php');
  });

  $('#groupList').on('click', function(event) {
    var submitButton = $('input[type="submit"]');
    var eventTarget = $(event.target);
    if (eventTarget.prop('tagName') === "H3") {
      eventTarget = eventTarget.parent();
    }
    if (eventTarget.attr('id') === 'addGroupItem') {
      $('#modalTitle').attr("placeholder", "new group name");
      $('#userList').append("<li id='addGroupUser' class='userItem'><h3 id='plusIcon'>+</h3><h3 contenteditable='true'>add user</h3></li>")
      submitButton.show();
      openModal();
    } else if (eventTarget.hasClass('groupItem')) {
      var groupID = parseInt(eventTarget.attr('id'));
      $.ajax({
        url: "setGroup.php",
        type: "POST",
        data: {
          groupID: groupID,
        },
        success: function(data) {
          window.location = 'groupPage.php';
        }
      });
    }
  });

  $('#addGroup').on('click', function(event) {
    event.preventDefault();
    var groupName = $('#modalTitle').html();
    if (groupName === "") {
      $('#errorMessage').html("Please choose a group name");
      shakeMessage();
      return;
    }

    var users = [];
    $(".userItem").each(function() {
        if ($(this).attr('id') !== 'addGroupUser') {
          users.push($(this).children('h3').eq(0).html().substring(1));
        }
    });

    $.ajax({
      url: "groupCheck.php",
      type: "POST",
      data: {
        users: users,
      },
      success: function(data) {
        if (data) {
          $('#errorMessage').html("Could not find user '@" + data + "'");
          shakeMessage();
        } else {
          $.ajax({
            url: "createGroup.php",
            type: "POST",
            data: {
              groupName: groupName,
              users: users,
            },
            success: function(data) {
              var groupID = parseInt(data);
              var htmlString = "<li class='groupItem' id='" + groupID + "gID'><h3>"+ groupName + "</h3></li>";
              $('#addGroupItem').before(htmlString);
              closeModal(function() {
                resetModal();
              });
            }
          });
        }
      }
    });
  });

  $('#userList').on('click', function(event) {
    var target = $(event.target);
    if (target.prop("tagName") === "H3") {
      if (target.attr('contentEditable') === "true") {
        (target.parent()).addClass('userItemActive');
        if (target.parent().attr('id') === 'addGroupUser') {
          target.html('');
        }
      }
    } else if (target.attr('id') === 'addGroupUser') {
      target.addClass('userItemActive');
      target.children('h3').eq(1).html('');
      target.children('h3').eq(1).focus();
    } else if (target.attr('class') === 'deleteTask') {
      setTimeout(function() {
        var user = target.parent();
        user.remove();
      }, 1);
    }
  });

  $('#userList').focusout(function(event) {
    var target = $(event.target);
    target.parent().removeClass('userItemActive');
    if (target.parent().attr('id') === "addGroupUser") {
      if (target.html() && target.html().trim() !== "add user") {
        target.parent().before("<li class='userItem'><h3 contenteditable='false'>@" + target.html() + "</h3><span class='deleteTask'>x</span></li>");
      }
      target.html("add user");
    }
  });

  $('.accept').on('click', function(event) {
    var groupID = parseInt($(event.target).parent().parent().attr('id'));
    var groupName = $(event.target).closest('.requestName').html();
    $.ajax({
      url: "handleRequest.php",
      type: "POST",
      data: {
        groupID: groupID,
        hasAccepted: 1
      },
      success: function(data) {
        var requestDivID = "#" + groupID + "rID";
        $(requestDivID).remove();
        $('#addGroupItem').before("<li class='groupItem' id='" + groupID + "'gID'><h3>"+ "test" + "</h3></li>");
        displayIndexGraph();
      }
    });
  });

  $('.reject').on('click', function(event) {
    var groupID = parseInt($(event.target).parent().parent().attr('id'));
    $.ajax({
      url: "handleRequest.php",
      type: "POST",
      data: {
        groupID: groupID,
        hasAccepted: -1
      },
      success: function(data) {
        var requestDivID = "#" + groupID + "rID";
        $(requestDivID).remove();
        displayIndexGraph();
      }
    });
  });

  $(document).keypress(function(event) {
    var target = $(event.target);
    if (target.parent().hasClass('userItem') || target.attr('name') === "username") {
      if (event.which < 48 || (event.which > 57 && event.which < 65) || (event.which > 90 && event.which < 97) || event.which > 122) {
        event.preventDefault();
      }
    } else if (target.attr('id') === 'newBillAmount') {
      if (event.which < 48 || event.which > 57 || event.which === 8) {
        event.preventDefault();
      }
    } else if (target.attr('name') === "email" && event.which === 32) {
      event.preventDefault();
    }

    if (event.which === 13) {
      event.preventDefault();
      if (target.attr('id') === 'modalTitle' && target.html() !== "") {
        $('#addGroupUser').children('h3').eq(1).html('');
        $('#addGroupUser').children('h3').eq(1).focus();
      } else if (target.attr('id') === 'newBillName' && target.html() !== "") {
        $('#newBillAmount').focus();
      } else if (target.attr('id') === 'newBillAmount' && target.html() !== "") {
        $(this).focusout();
        if ($('#payBill').is(':hidden')) {
          $('#addBill').click();
        } else {
          $('#payBill').click();
        }
      } else if (target.attr('id') === 'newUsername' && target.html() !== "") {
        $('#addUser').click();
      } else if (target.parent().attr('id') === 'addGroupUser' && target.html() !== "") {
        target.parent().before("<li class='userItem'><h3 contenteditable='false'>@" + target.html() + "</h3><span class='deleteTask'>x</span></li>");
        target.html('');
      } else if (!target.next().is(':visible')) {
        $('#webappLogin').click();
      } else if (target.hasClass('loginInput') || target.hasClass('registerInput')) {
        target.next().focus();
      } else if (target.attr('id').endsWith('gID')) {
        target.blur();
      } else if (target.attr('id') === 'billName') {
        target.blur();
      }
    }
  });

  $('.close').on('click', function() {
    closeModal(function() {
      resetModal();
    });
  });

  function resetModal() {
    setTimeout(function() {
      $('#userList').empty();
      $('#modalTitle').html('');
      $('#errorMessage').html('');
      $('#newBillName').html('');
      $('#newUsername').html('');
      $('#newBillAmount').html('0');
      $('#newBillName').show();
      $('#newBillLine').show();
      $('#newUsernameLine').show();
      $('#addBill').show();
      $('#addUser').show();
      $('#payBill').show();
    }, 500);
  }

  function openModal() {
    modal.css('opacity', '1');
    modal.css('z-index', '1');
  }

  function closeModal(callback) {
    modal.css('opacity', '0');
    setTimeout(function() {
      modal.css('z-index', '-1');
    }, 500);
    typeof callback == "function" && callback();
  }

  $('.navBarItemL').on('click', function() {
    window.location.replace("index.php");
  })

  $('#groupUserList').on('click', function(event) {
    var target = $(event.target);
    if (target.prop('tagName') === "H3") {
      target = target.parent();
    }
    if (target.attr('id') === 'addUserItem') {
      $('#newBillName').hide();
      $('#newBillLine').hide();
      $('#addBill').hide();
      $('#payBill').hide();
      openModal();
    } else if (target.attr('id') === 'leaveGroup') {
      var gID = parseInt($("h2[id$='gID']").attr('id'));
      $.ajax({
        url: "leaveGroup.php",
        type: "POST",
        data: {
          gID: gID,
        },
        success: function(data) {
          if (data) {
            console.log(data);
            target.children('h3').eq(0).html('cannot leave until all pending bills are paid');
          } else {
            window.location.replace('index.php');
          }
        }
      });
    }
  });

  $("h2[id$='gID']").focusout(function(event) {
    var groupID = parseInt($(this).attr('id'));
    var groupName = $(this).html();
    $.ajax({
      url: "updateGroupName.php",
      type: "POST",
      data: {
        groupID: groupID,
        groupName: groupName
      },
      success: function(data) {
        // Emails sent
      }
    });
  });

  $('#addUser').on('click', function(event) {
    event.preventDefault();
    var users = [];
    if ($('#newUsername').html() === '') { return; }
    $(".groupUserItem").each(function() {
      if ($(this).attr('id') !== 'addUserItem') {
        users.push($(this).children('h3').eq(0).html().substring(1));
      }
    });
    var username = $('#newUsername').html();
    $.ajax({
      url: "addGroupUser.php",
      type: "POST",
      data: {
        username: username,
        users: users,
      },
      success: function(data) {
        if (data) {
          var userID = parseInt(data);
          var listItem = "<li class='groupUserItem pendingUser' id='" + userID + "uID'><h3>@" + username + "</h3><h3 class='userStatus'>pending</h3></li>"
          $('#addUserItem').before(listItem);
          closeModal(function() {
            resetModal();
          });
        } else {
          $('#errorMessage').html('could not add user');
          shakeMessage();
        }
      }
    });
  });

  $('#addBill').on('click', function(event) {
    event.preventDefault();
    var billName = $('#newBillName').html();
    var billAmount = $('#newBillAmount').html();
    var users = [];
    $(".groupUserItem").each(function() {
      var id = parseInt($(this).attr('id'));
      if (!isNaN(id)) {
        users.push(id);
      }
    });

    if (billName === '') {
      $('#errorMessage').html('Please enter a bill name');
    } else if (billAmount === '') {
      $('#errorMessage').html('Please enter the bill value');
    } else {
      $.ajax({
        url: "createBill.php",
        type: "POST",
        data: {
          billName: billName,
          billAmount: billAmount,
          users: users
        },
        success: function(data) {
          var billID = parseInt(data);
          var listItem = "<li id='" + billID + "bID' class='billItem'><h3>" + billName + "</h3><span class='billAmount'>&pound;" + billAmount + "</span></li>"
          $('#addBillItem').before(listItem);
          closeModal(function() {
            $('#leftButton').click();
            resetModal();
          });
        }
      });
    }
  });

  $('.resend').on('click', function(event) {
    var userID = parseInt($(this).parent().attr('id'));
    $.ajax({
      url: "resendGroupRequest.php",
      type: "POST",
      data: {
        userID: userID,
      },
      success: function(data) {
        $(event.target).prev().html('pending');
        $(event.target).parent().addClass('rejectedUser').removeClass('pendingUser');
        $(event.target).remove();
      }
    });
  });

  $('#billList').on('click', function(event) {
    var eventTarget = $(event.target);

    if (eventTarget.attr('id') === 'payMessage') {
      $('#newBillName').hide();
      $('#newUsernameLine').hide();
      $('#addBill').hide();
      $('#addUser').hide();
      openModal();
    }

    if (eventTarget.prop('tagName') === "H3") {
      eventTarget = eventTarget.parent();
    } else if (eventTarget.prop('tagName') === "SPAN") {
      eventTarget = eventTarget.parent();
    }

    if (eventTarget.attr('id') === 'addBillItem') {
      $('#newUsernameLine').hide();
      $('#addUser').hide();
      $('#payBill').hide();
      openModal();
    } else if (eventTarget.hasClass('billItem')) {
      if (eventTarget.next().hasClass('billDetails')) {
        eventTarget.next().remove();
        detailIsHidden = true;
      } else {
        if (!detailIsHidden) {
          $('.billDetails').remove();
        }
        var billID = parseInt(eventTarget.attr('id'));
        var billName = $(eventTarget).children('h3').eq(0).html();
        var billDetailString = "<li class='billDetails'><h3 id='billName' class='contentHeader' contenteditable='true'>" + billName + "</h3><div id='chartContainer'><div id='leftChartContainer'><h4 class='chartLabels'>individual status</h3><canvas id='individualBillChart'></canvas><h4 id='billsPaidIndividual' class='chartDescriptionLeft'>bills paid: &pound;</h3><h4 id='billsPendingIndividual' class='chartDescriptionLeft'>bills pending: &pound;</h3></div><div id='rightChartContainer'><h4 class='chartLabels'>group status</h3><canvas id='groupBillChart'></canvas><h4 id='billsPaidGroup' class='chartDescriptionRight'>bills paid: &pound;</h3><h4 id='billsPendingGroup' class='chartDescriptionRight'>bills pending: &pound;</h3></div></div><h3 id='payMessage'>pay now</h3><h3 id='billMessage'>your individual bill has been paid</h3></li>";
        getCharts(billID);
        eventTarget.after(billDetailString);
        detailIsHidden = false;
      }
    }
  });

  $('#billList').focusout(function() {
    var eventTarget = $(event.target);
    if (eventTarget.attr('id') === 'billName') {
      var billID = parseInt($(eventTarget).parent().prev().attr('id'));
      var billName = $(eventTarget).html();
      $.ajax({
        url: "updateBillName.php",
        type: "POST",
        data: {
          billID: billID,
          billName: billName
        },
        success: function(data) {
          $(eventTarget).parent().prev().children('h3').eq(0).html(billName);
        }
      });
    }
  })

  function displayIndexGraph() {
    $.ajax({
      url: "getIndividualSummary.php",
      type: "POST",
      dataType: 'json',
      success: function(data) {
        var dataValues = [];
        $.each(data, function(index, element) {
          dataValues.push(element);
        });
        var billsPaid = dataValues[0];
        if (dataValues.every( (val, i, arr) => val === arr[0] )) {
          dataValues[0] = 1;
        }

        document.getElementById("individualSummary").height = $('#individualSummary').width() * 0.6;
        new Chart(document.getElementById("individualSummary"), {
          type: 'doughnut',
          data: {
            labels: ["Bills Paid", "Bills Pending", "Bills Requested"],
            datasets: [
              {
                backgroundColor: ["#4EA6EA", "#798889","#FF9900"],
                data: dataValues
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            cutoutPercentage: 90,
            width: 2
          }
        });

        var total = dataValues[0] + dataValues[1] + dataValues[2];
        var paidPercentage = Math.round(100 * billsPaid / (total));
        var pendingPercentage = Math.round(100 * dataValues[1] / (total));
        var requestedPercentage = Math.round(100 * dataValues[2] / (total));
        $('#summaryPaid').html("&pound;" + billsPaid + " (" + paidPercentage + "%)");
        $('#summaryPending').html("&pound;" + dataValues[1] + " (" + pendingPercentage + "%)");
        $('#summaryRequested').html("&pound;" + dataValues[2] + " (" + requestedPercentage + "%)");
      }
    });
  }

  function getCharts(billID) {
    // Get Individual Bill Chart
    $.ajax({
        url: "getIndividualBillChart.php",
        type: "POST",
        data: {
            billID: billID
        },
        dataType: 'json',
        success: function(data) {
            var amountPaid = data.amountPaid;
            var amountPending = data.amountToPay - amountPaid;
            // Display individual chart
            document.getElementById("individualBillChart").height = $('#individualBillChart').width() * 0.5;
            new Chart(document.getElementById("individualBillChart"), {
                type: 'doughnut',
                data: {
                    labels: ["Bills Paid", "Bills Pending"],
                    datasets: [
                        {
                            backgroundColor: ["#4EA6EA", "#798889"],
                            data: [amountPaid, amountPending]
                        }
                    ]
                },
                options: {
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 90,
                    width: 2
                }
            });
            $('#billsPaidIndividual').html($('#billsPaidIndividual').html() + amountPaid);
            $('#billsPendingIndividual').html($('#billsPendingIndividual').html() + amountPending);

            if (amountPending === 0) {
              $('#payMessage').hide();
            } else {
              $('#billMessage').hide();
            }
        }
    });

    // Get Group Bill Chart
    $.ajax({
        url: "getGroupBillChart.php",
        type: "POST",
        data: {
            billID: billID
        },
        dataType: 'json',
        success: function(data) {
          var totalPaid = 0;
          var totalPending = 0;
            var labels = [];
            var dataValues = [];
            $.each(data, function(index, element) {
              labels.push(index + ", paid");
              labels.push(index + ", pending");
              dataValues.push(element[0]);
              dataValues.push(element[1]);
              totalPaid += element[0];
              totalPending += element[1];
            });
            // Display individual chart
            document.getElementById("groupBillChart").height = $('#groupBillChart').width() * 0.5;
            new Chart(document.getElementById("groupBillChart"), {
              type: 'doughnut',
              data: {
                labels: labels,
                datasets: [
                  {
                    backgroundColor: ["rgba(34,167,240,1)", "rgba(34,167,240,0.4)", "rgba(103,103,102,1)", "rgba(103,103,102,0.4)", "rgba(240,101,47,1)", "rgba(240,101,47,0.4)", "rgba(254,204,70,1)", "rgba(254,204,70,0.4)", "rgba(210,82,127,1)", "rgba(210,82,127,0.4)"],
                    data: dataValues
                  }
                ]
              },
              options: {
                legend: {
                  display: false
                },
                cutoutPercentage: 90,
                width: 2
              }
            });

            $('#billsPaidGroup').html($('#billsPaidGroup').html() + totalPaid);
            $('#billsPendingGroup').html($('#billsPendingGroup').html() + totalPending);
            if (totalPending === 0) {
              $('#billMessage').html('your group has paid this bill');
            }
        }
    });
  }

  $('#leftButton').on('click', function() {
    $.ajax({
      url: "getIndividualChart.php",
      type: "POST",
      dataType: 'json',
      success: function(data) {
        var amountPaid = data.paidTotal;
        var amountPending = data.total - amountPaid;
        var paidDisplay = amountPaid;
        if (amountPaid === 0 && amountPending === 0) {
          paidDisplay = 1;
        }
        document.getElementById("individualChart").height = $('#individualChart').width() * 0.6;
        new Chart(document.getElementById("individualChart"), {
          type: 'doughnut',
          data: {
              labels: ["Bills Paid", "Bills Pending"],
              datasets: [
              {
                backgroundColor: ["#4EA6EA", "#798889"],
                data: [paidDisplay, amountPending]
              }
            ]
          },
          options: {
            legend: {
                display: false
            },
            cutoutPercentage: 90,
            width: 2
          }
        });
        var paidPercentage = Math.round(100 * amountPaid / (paidDisplay + amountPending));
        var pendingPercentage = Math.round(100 * amountPending / (paidDisplay + amountPending));
        $('#largeBillPaid').html("&pound;" + amountPaid + " (" + paidPercentage + "%)");
        $('#largeBillPending').html("&pound;" + amountPending + " (" + pendingPercentage + "%)");
      }
    });
  });

  $('#rightButton').on('click', function() {
    $.ajax({
      url: "getGroupChart.php",
      type: "POST",
      dataType: 'json',
      success: function(data) {
        var paidTotal = 0;
        var pendingTotal = 0;
        var labels = [];
        var dataValues = [];
        $.each(data, function(index, element) {
            labels.push(index + ", paid");
            labels.push(index + ", pending");
            dataValues.push(element[0]);
            dataValues.push(element[1]);
            paidTotal += element[0];
            pendingTotal += element[1];
        });
        var total = paidTotal + pendingTotal;
        if (dataValues.every( (val, i, arr) => val === arr[0] )) {
          dataValues[0] = 1;
          total += 1;
        }

        document.getElementById("individualChart").height = $('#individualChart').width() * 0.6;
        new Chart(document.getElementById("individualChart"), {
          type: 'doughnut',
          data: {
            labels: labels,
            datasets: [
              {
                backgroundColor: ["#4EA6EA", "#798889"],
                data: dataValues
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            cutoutPercentage: 90,
            width: 2
          }
        });

        var paidPercentage = Math.round(100 * paidTotal / total);
        var pendingPercentage = Math.round(100 * pendingTotal / total);
        $('#largeBillPaid').html("&pound;" + paidTotal + " (" + paidPercentage + "%)");
        $('#largeBillPending').html("&pound;" + pendingTotal + " (" + pendingPercentage + "%)");
      }
    });
  });

  $('#payBill').on('click', function() {
    event.preventDefault();
    var payment = parseInt($('#newBillAmount').html());
    var billID = parseInt($('.billDetails').eq(0).prev().attr('id'));
    if (isNaN(payment) || payment === 0) {
      $('#errorMessage').html('please enter a valid amount');
      shakeMessage();
      return;
    }
    $.ajax({
      url: "payBill.php",
      type: "POST",
      data: {
        payment: payment,
        billID: billID
      },
      success: function(data) {
        if (data) {
          $('#errorMessage').html(data);
          shakeMessage();
        } else {
          closeModal();
          getCharts(billID);
        }
      }
    });
  });

  // Display graphs onload
  if ($('#individualSummary').length) {
    displayIndexGraph();
  } else if ($('#leftButton').length) {
    $('#leftButton').click();
  }

});
