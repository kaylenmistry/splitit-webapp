<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID;
    $firstname = $db->escapeString($_POST["firstname"]);
    $lastname = $db->escapeString($_POST["lastname"]);
    $email = $db->escapeString($_POST["email"]);
    $username = $db->escapeString($_POST["username"]);
    $password = $db->escapeString($_POST["password"]);
    $confirmPassword = $db->escapeString($_POST["confirmPassword"]);

    if (empty($firstname) || empty($lastname)) {
        echo "Please enter your name";
        return;
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Please enter a valid email address";
        return;
    } elseif (empty($username)) {
        echo "Please enter a username";
        return;
    } elseif (empty($password)) {
        echo "Please enter a password";
        return;
    } elseif ($password !== $confirmPassword) {
        echo "Passwords must match";
        return;
    }

    $existingUserQuery = $db->prepare("SELECT * FROM users WHERE username=:username LIMIT 1");
    $existingUserQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $result = $existingUserQuery->execute();
    if ($result->fetchArray()) {
        echo "Username already exists";
        return;
    }

    $salt = sha1($username);
    $passwordHash = sha1($salt.$password);

    $registrationQuery = $db->prepare("INSERT INTO users VALUES (NULL, :firstname, :lastname, :username, :email, :passwordHash)");
    $registrationQuery->bindValue(':firstname', $firstname, SQLITE3_TEXT);
    $registrationQuery->bindValue(':lastname', $lastname, SQLITE3_TEXT);
    $registrationQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $registrationQuery->bindValue(':email', $email, SQLITE3_TEXT);
    $registrationQuery->bindValue(':passwordHash', $passwordHash, SQLITE3_TEXT);
    $result = $registrationQuery->execute();

    $result = $existingUserQuery->execute();
    if ($row = $result->fetchArray()) {
        $userID = $row['userID'];
    }
    $_SESSION["username"] = $username;
    $_SESSION["userID"] = $userID;
    $_SESSION["firstname"] = $firstname;
    $_SESSION["email"] = $email;
?>
