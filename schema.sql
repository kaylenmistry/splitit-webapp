CREATE TABLE IF NOT EXISTS users (
    userID integer PRIMARY KEY,
    firstName varchar(40),
    lastName varchar(40),
    username varchar(40) NOT NULL UNIQUE,
    email varchar(40) NOT NULL UNIQUE,
    passwordHash varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS bills (
    billID integer PRIMARY KEY,
    groupID integer,
    billName varchar(40),
    billAmount integer,
    FOREIGN KEY (groupID) REFERENCES groups (groupID)
    ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS groups (
  groupID integer PRIMARY KEY,
  groupName varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS userGroups (
  userID integer,
  groupID integer,
  hasAccepted integer,
  PRIMARY KEY (userID, groupID),
  FOREIGN KEY (userID) REFERENCES users (userID)
  ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (groupID) REFERENCES groups (groupID)
  ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS groupRequests (
    recipientID integer,
    groupID integer,
    authorUsername varchar(40) NOT NULL,
    PRIMARY KEY (recipientID, groupID),
    FOREIGN KEY (recipientID) references users (userID)
    ON DELETE CASCADE ON UPDATE NO ACTION,
    FOREIGN KEY (groupID) REFERENCES groups (groupID)
    ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS userBills (
    userID integer,
    billID integer,
    amountToPay integer,
    amountPaid integer,
    PRIMARY KEY (userID, billID),
    FOREIGN KEY (userID) REFERENCES users (userID)
    ON DELETE CASCADE ON UPDATE NO ACTION,
    FOREIGN KEY (billID) REFERENCES groups (billID)
    ON DELETE CASCADE ON UPDATE NO ACTION
);