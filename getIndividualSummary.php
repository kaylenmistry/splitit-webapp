<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION['userID'];

    $selectQuery = $db->prepare("SELECT * FROM userBills INNER JOIN bills ON userBills.billID=bills.billID LEFT OUTER JOIN groupRequests ON bills.groupID=groupRequests.groupID AND userBills.userID=groupRequests.recipientID WHERE userBills.userID=:userID");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    
    $billsPaid = 0;
    $billsPending = 0;
    $billsRequested = 0;
    while($row = $result->fetchArray()) {
        if ($row['authorUsername']) {
            $billsRequested += $row['amountToPay'];
        } else {
            $billsPaid += $row['amountPaid'];
            $billsPending += ($row['amountToPay'] - $row['amountPaid']);
        }
    }
    echo json_encode(array("paid" => $billsPaid, "pending" => $billsPending, "requested" => $billsRequested));
?>