<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION['userID'];
    $groupID = $_POST['gID'];

    $selectQuery = $db->prepare("SELECT SUM(amountPaid) AS totalPaid, SUM(amountToPay) AS totalToPay FROM userBills INNER JOIN bills ON userBills.billID=bills.billID WHERE userID=:userID AND groupID=:groupID");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    if ($row = $result->fetchArray()) {
        $totalPaid = $row['totalPaid'];
        $totalToPay = $row['totalToPay'];
        if ($totalPaid < $totalToPay) {
            echo "you must pay all pending bills before leaving a group"; 
        }
    }

    $deleteQuery = $db->prepare("DELETE FROM userGroups WHERE userID=:userID AND groupID=:groupID");
    $deleteQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $deleteQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();

    $selectQuery = $db->prepare("SELECT * FROM userGroups INNER JOIN groups ON userGroups.groupID=groups.groupID WHERE groups.groupID=:groupID LIMIT 1");
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    if ($row = $result->fetchArray()) {
        $deleteQuery = $db->prepare("DELETE FROM groups WHERE groupID=:groupID");
        $deleteQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
        $result = $deleteQuery->execute();
    }
?>
