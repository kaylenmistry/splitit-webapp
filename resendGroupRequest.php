<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $username = $_SESSION['username'];
    $groupID = $_SESSION['groupID'];
    $recipientID = $_POST['userID'];

    echo $username;
    echo $groupID;
    echo $recipientID;

    $insertQuery = $db->prepare("INSERT INTO groupRequests VALUES (:recipientID, :groupID, :username)");
    $insertQuery->bindValue(':recipientID', $recipientID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    $updateQuery = $db->prepare("UPDATE userGroups SET hasAccepted=0 WHERE userID=:recipientID AND groupID=:groupID");
    $updateQuery->bindValue(':recipientID', $recipientID, SQLITE3_INTEGER);
    $updateQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>
