<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $username = $_POST["username"];
    $password = $_POST["password"];
    if (empty($username)) {
        echo "Please enter a username";
        return;
    } elseif (empty($password)) {
        echo "Please enter a password";
        return;
    }


    $salt = sha1($username);
    $passwordHash = sha1($salt.$password);

    $getUserQuery = $db->prepare("SELECT * FROM users WHERE username=:username AND passwordHash=:passwordHash LIMIT 1");
    $getUserQuery->bindValue(':username', $username, SQLITE3_TEXT);
    $getUserQuery->bindValue(':passwordHash', $passwordHash, SQLITE3_TEXT);
    $result = $getUserQuery->execute();
    if ($row = $result->fetchArray()) {
        $_SESSION["userID"] = $row["userID"];
        $_SESSION["username"] = $row["username"];
        $_SESSION["firstname"] = $row["firstName"];
        $_SESSION["email"] = $row["email"];
    } else {
        echo "Incorrect username or password";
    }
?>
