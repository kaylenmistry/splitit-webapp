<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $groupID = $_POST["groupID"];
    $hasAccepted = $_POST["hasAccepted"];
    echo $userID ." ".$groupID;

    $deleteQuery = $db->prepare("DELETE FROM groupRequests WHERE recipientID=:userID AND groupID=:groupID");
    $deleteQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $deleteQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $deleteQuery->execute();

    $updateQuery = $db->prepare("UPDATE userGroups SET hasAccepted=:hasAccepted WHERE userID=:userID AND groupID=:groupID");
    $updateQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $updateQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $updateQuery->bindValue(':hasAccepted', $hasAccepted, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>
