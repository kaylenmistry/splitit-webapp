<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $groupID = $_SESSION["groupID"];

    $selectQuery = $db->prepare("SELECT * FROM bills where groupID=:groupID");
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    while($row = $result->fetchArray()) {
        $billID = $row['billID'];
        $billName = $row['billName'];
        $billAmount = $row['billAmount'];
        $htmlString = "<li id='".$billID."bID'class='billItem'><h3>".$billName."</h3><span class='billAmount'>&pound;".$billAmount."</span></li>";
        echo $htmlString;
    }
?>
