<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();
    
    if (!isset($_POST["users"])) {
      return;
    }
    $otherUsers = $_POST["users"];

    foreach ($otherUsers as $user) {
      $selectQuery = $db->prepare("SELECT userID FROM users WHERE username=:username LIMIT 1");
      $selectQuery->bindValue(':username', $user, SQLITE3_TEXT);
      $result = $selectQuery->execute();
      $userID = $result->fetchArray();
      $userID = $userID['userID'];

      if (!$userID) {
        echo $user;
        return;
      }
    }
?>
