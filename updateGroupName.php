<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $groupID = $_POST['groupID'];
    $groupName = $_POST['groupName'];

    $updateQuery = $db->prepare("UPDATE groups SET groupName=:groupName WHERE groupID=:groupID");
    $updateQuery->bindValue(':groupName', $groupName, SQLITE3_TEXT);
    $updateQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>