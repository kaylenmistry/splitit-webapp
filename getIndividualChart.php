<?php 
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $groupID = $_SESSION["groupID"];

    $selectQuery = $db->prepare("SELECT * FROM userBills INNER JOIN bills ON userBills.billID=bills.billID WHERE userID=:userID AND groupID=:groupID");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    $total = 0;
    $paidTotal = 0;

    while ($row = $result->fetchArray()) {
        $total += $row['amountToPay'];
        $paidTotal += $row['amountPaid'];
    }

    echo json_encode(array("paidTotal" => $paidTotal, "total" => $total));
?>