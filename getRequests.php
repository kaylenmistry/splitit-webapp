<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];

    $selectQuery = $db->prepare("SELECT * FROM groupRequests INNER JOIN groups ON groupRequests.groupID = groups.groupID WHERE recipientID=:userID");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    
    $floatRight = false;
    
    while ($row = $result->fetchArray()) {
        $groupID = $row["groupID"];
        $selectQuery = $db->prepare("SELECT COUNT(*) AS count FROM userGroups WHERE groupID=:groupID");
        $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
        $countResult = ($selectQuery->execute());
        $groupSize = ($countResult->fetchArray());

        $selectQuery = $db->prepare("SELECT SUM(amountPaid) AS totalPaid, SUM(amountToPay) AS total FROM userBills INNER JOIN bills ON userBills.billID=bills.billID WHERE groupID=:groupID AND userID=:userID");
        $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
        $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
        $sumResult = ($selectQuery->execute());
        $billCost = ($sumResult->fetchArray());
        $billCost = $billCost['total'] - $billCost['totalPaid'];
        if (!$billCost) { $billCost = 0; }
        $groupSize = ($groupSize['count']) - 2;
        $class = "groupRequest";

        if ($floatRight) { $class .= " rightRequest"; }
        $groupName = $row["groupName"];
        $authorUsername = $row["authorUsername"];

        $htmlString = "<div id='".$groupID."rID' class='".$class."'>
                        <h3 class='requestName'>".$groupName."</h3>
                        <h2 class='requestCost'>&pound;".$billCost."</h2>
                        <p class='requestInfo'>(@".$authorUsername." and ".$groupSize." others)</p>
                        <div class='requestActions'>
                            <span class='accept'>accept</span><span class='reject'>reject</span>
                        </div>
                    </div>";
        echo $htmlString;
        $floatRight = !$floatRight;
    }
?>
