<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $billID = $_POST['billID'];
    $billName = $_POST['billName'];

    $updateQuery = $db->prepare("UPDATE bills SET billName=:billName WHERE billID=:billID");
    $updateQuery->bindValue(':billName', $billName, SQLITE3_TEXT);
    $updateQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
    $result = $updateQuery->execute();
?>
