<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];
    $groupName = $_POST["groupName"];

    $insertQuery = $db->prepare("INSERT INTO groups VALUES (NULL, :groupName)");
    $insertQuery->bindValue(':groupName', $groupName, SQLITE3_TEXT);
    $result = $insertQuery->execute();

    $rowID = $db->lastInsertRowID();
    $selectQuery = $db->prepare("SELECT groupID FROM groups WHERE rowid=:rowID LIMIT 1");
    $selectQuery->bindValue(':rowID', $rowID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $groupID = $result->fetchArray();
    $groupID = $groupID['groupID'];

    $insertQuery = $db->prepare("INSERT INTO userGroups VALUES (:userID, :groupID, 1)");
    $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $insertQuery->execute();

    if (!isset($_POST["users"])) {
      echo $groupID;
      return;
    }
    $otherUsers = $_POST["users"];

    foreach ($otherUsers as $user) {
      // Get author's username
      $selectQuery = $db->prepare("SELECT username FROM users WHERE userID=:userID LIMIT 1");
      $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
      $result = $selectQuery->execute();
      $username = $result->fetchArray();
      $username = $username['username'];
      // Get recipients userID
      $selectQuery = $db->prepare("SELECT userID FROM users WHERE username=:username LIMIT 1");
      $selectQuery->bindValue(':username', $user, SQLITE3_TEXT);
      $result = $selectQuery->execute();
      $userID = $result->fetchArray();
      $userID = $userID['userID'];

      // Insert recipient into group
      $insertQuery = $db->prepare("INSERT INTO userGroups VALUES (:userID, :groupID, 0)");
      $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
      $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
      $result = $insertQuery->execute();
      // Insert group request
      $insertQuery = $db->prepare("INSERT INTO groupRequests VALUES (:userID, :groupID, :username)");
      $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
      $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
      $insertQuery->bindValue(':username', $username, SQLITE3_TEXT);
      $result = $insertQuery->execute();
    }

    // TODO: email requests

    echo $groupID;
?>
