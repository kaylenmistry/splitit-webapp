<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $groupID = $_SESSION['groupID'];

    $selectQuery = $db->prepare("SELECT groupName FROM groups WHERE groupID=:groupID LIMIT 1");
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $result = $result->fetchArray();

    $groupName = $result['groupName'];

    $htmlString = "<h2 contentEditable='true' class='contentHeader' id='".$groupID."gID'>".$groupName."</h2>";
    echo $htmlString;
?>