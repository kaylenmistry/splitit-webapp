<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $userID = $_SESSION["userID"];

    $selectQuery = $db->prepare("SELECT * FROM userGroups INNER JOIN groups ON userGroups.groupID = groups.groupID WHERE userID=:userID AND hasAccepted=1");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    while ($row = $result->fetchArray()) {
        $groupID = $row['groupID'];
        $groupName = $row['groupName'];

        $htmlString = "<li class=\"groupItem\" id=\"".$groupID."gID\">
                        <h3>".$groupName."</h3>
                      </li>";
        echo $htmlString;
    }
?>
