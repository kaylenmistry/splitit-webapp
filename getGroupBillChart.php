<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();
    $billID = $_POST["billID"];

    $selectQuery = $db->prepare("SELECT * FROM users INNER JOIN userBills ON users.userID = userBills.userID WHERE billID=:billID");
    $selectQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    $data = array();
    while ($row = $result->fetchArray()) {
        $username = $row['username'];
        $amountPaid = $row['amountPaid'];
        $amountPending = $row['amountToPay'] - $amountPaid;

        $values = array($amountPaid, $amountPending);
        $data[$username] = $values;
    }
    echo json_encode($data);

?>
