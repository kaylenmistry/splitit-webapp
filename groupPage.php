<?php
  session_start();
  if (!isset($_SESSION["userID"]) || !isset($_SESSION["username"])) {
    header("Location: landing.php");
    die();
  } else if (!isset($_SESSION["groupID"])) {
    header("Location: index.php");
    die();
  }
?>
<!doctype html>
<html id="standardHTML">
  <head>
    <title>splitit</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
  </head>
  <body>
    <div class="navBar">
      <ul>
        <li class="navBarItemL"><p><strong>splitit</strong></p></li>
        <li class="navBarItemR">
          <button id="logoutButton" class="navBarButton">
            <p>logout</p>
          </button>
        </li>
      </ul>
    </div>
    <div id="contentContainer">
      <div id="leftColumn">
        <div id="userSummary">
          <?php include 'getGroupName.php'; ?>
          <h3 id="statusButtons"><span id="leftButton">individual status</span><span id="rightButton">group status</span></h3>
          <canvas id="individualChart"></canvas>
          <div id="topStatus">
            <h3 class="billLabel">bills paid:</h3>
            <h3 id="largeBillPaid" class="billValue">&pound;140 (70%)</h3>
          </div>
          <div>
            <h3 class="billLabel">bills pending:</h3>
            <h3 id="largeBillPending" class="billValue">&pound;40 (20%)</h3>
          </div>
          <div>
          </div>
          <p id="scrollMessage">scroll down to see the group members</p>
        </div>
        <div id="requestsSummary">
          <h2 class="contentHeader" id="requestsHeader">group members</h2>
        </div>
        <div id="userContainer">
          <ul id="groupUserList">
            <?php include 'getGroupUsers.php'; ?>
            <li id="addUserItem" class="groupUserItem">
              <h3>+ add user</h3>
            </li>
            <li id="leaveGroup" class="groupUserItem">
              <h3>leave group</h3>
            </li>
        </ul>
        </div>
      </div>
      <div id="billContainer">
        <ul id="billList">
          <?php include 'getBills.php'; ?>
          <li id="addBillItem" class="billItem">
            <h3>+ add bill</h3>
          </li>
        </ul>
      </div>
    </div>
    <div id="modal" class="closed">
      <div class="modalContent">
        <span class="close">&times;</span>
        <h1 id="newBillName" contenteditable="true" placeholder="new bill name"></h1>
        <span id="newUsernameLine"class="together"><h1 id="atSign">@</h1><h1 id="newUsername" contenteditable="true" placeholder="username"></h1></span>
        <p id="errorMessage"></p>
        <span id="newBillLine" class="together"><h1 id="poundSign">&pound;</h1><h1 id="newBillAmount" contenteditable="true" placeholder="0" type="number"></h1></span>
        <form>
          <input id="addBill" type="submit" value="add bill"/>
          <input id="addUser" type="submit" value="add user"/>
          <input id="payBill" type="submit" value="pay amount"/>
        </form>
      </div>
    </div>
  </body>
</html>
