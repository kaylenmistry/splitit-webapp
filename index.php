<?php
  session_start();
  if (!isset($_SESSION["userID"]) || !isset($_SESSION["username"])) {
    header("Location: landing.php");
    die();
  }
?>
<!doctype html>
<html id="standardHTML">
  <head>
    <title>splitit</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
  </head>
  <body>
    <div class="navBar">
      <ul>
        <li class="navBarItemL"><p><strong>splitit</strong></p></li>
        <li class="navBarItemR">
          <button id="logoutButton" class="navBarButton">
            <p>logout</p>
          </button>
        </li>
      </ul>
    </div>
    <div id="contentContainer">
      <div id="leftColumnWide">
        <div id="userSummary">
          <h2 class="contentHeader" id="welcomeMessage">Welcome <?php echo $_SESSION["firstname"]; ?></h2>
          <h3 id="summaryMessage">here's your bill summary:</h3>
          <canvas id="individualSummary"></canvas>
          <div id="topStatus">
            <h3 class="billLabel">bills paid:</h3>
            <h3 id="summaryPaid" class="billValue">&pound;140 (70%)</h3>
          </div>
          <div>
            <h3 class="billLabel">bills pending:</h3>
            <h3 id="summaryPending" class="billValue">&pound;40 (20%)</h3>
          </div>
          <div>
            <h3 class="billLabel">bills requested:</h3>
            <h3 id="summaryRequested" class="billValue">&pound;10 (5%)</h3>
          </div>
          <p id="scrollMessage">scroll down to see your group requests</p>
        </div>
        <div id="requestsSummary">
          <h2 class="contentHeader" id="requestsHeader">group requests</h2>
        </div>
        <div id="requestContainer">
          <?php include 'getRequests.php'; ?>
        </div>
      </div>
      <div id="groupContainer">
        <ul id="groupList">
          <?php include 'getGroups.php'; ?>
          <li id="addGroupItem" class="groupItem">
            <h3>+ add group</h3>
          </li>
        </ul>
      </div>
    </div>
    <div id="modal" class="closed">
      <div class="modalContent">
        <span class="close">&times;</span>
        <h1 id="modalTitle" contenteditable="true" placeholder=""></h1>
        <p id="errorMessage"></p>
        <ul id='userList'></ul>
        <form>
          <input id="addGroup" type="submit" value="add group"/>
        </form>
      </div>
    </div>
  </body>
</html>
