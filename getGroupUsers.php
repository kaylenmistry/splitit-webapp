<?php 
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');

    $db = new Database();

    $groupID = $_SESSION["groupID"];

    $selectQuery = $db->prepare("SELECT * FROM users INNER JOIN userGroups ON users.userID = userGroups.userID WHERE groupID=:groupID");
    $selectQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    while ($row = $result->fetchArray()) {
        $userID = $row['userID'];
        $username = $row['username'];
        $hasAccepted = $row['hasAccepted'];
        $class = "";
        $status = "";
        if ($hasAccepted === 0) {
            $class = "pendingUser";
            $status = "<span class='userStatus'>pending</span>";
        } else if ($hasAccepted === -1) {
            $class = "rejectedUser";
            $status = "<span class='userStatus'>rejected</span><span class='resend'>resend</span>";
        }

        $htmlString = "<li class='groupUserItem ".$class."' id='".$userID."uID'>
                        <h3>@".$username."</h3>".$status.
                      "</li>";
        echo $htmlString;
    }
?>