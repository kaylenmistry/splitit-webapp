<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    session_start();
    require_once('database.php');

    $db = new Database();

    $groupID = $_SESSION["groupID"];
    $billName = $_POST["billName"];
    $billAmount = $_POST["billAmount"];
    $users = $_POST["users"];

    $insertQuery = $db->prepare("INSERT INTO bills VALUES (NULL, :groupID, :billName, :billAmount)");
    $insertQuery->bindValue(':groupID', $groupID, SQLITE3_INTEGER);
    $insertQuery->bindValue(':billName', $billName, SQLITE3_TEXT);
    $insertQuery->bindValue(':billAmount', $billAmount, SQLITE3_INTEGER);
    $result = $insertQuery->execute();

    $rowID = $db->lastInsertRowID();
    $selectQuery = $db->prepare("SELECT billID FROM bills WHERE rowid=:rowID LIMIT 1");
    $selectQuery->bindValue(':rowID', $rowID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();
    $billID = $result->fetchArray();
    $billID = $billID['billID'];

    $splitBillAmount = $billAmount / count($users); 
    $modulus = $billAmount % count($users);

    foreach($users as $userID) {
        $amount = $splitBillAmount;
        if ($modulus > 0) {
            $amount++;
            $modulus--;
        }
        // Insert the user bill into userBills
        $insertQuery = $db->prepare("INSERT INTO userBills VALUES (:userID, :billID, :amount, 0)");
        $insertQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
        $insertQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
        $insertQuery->bindValue(':amount', $amount, SQLITE3_INTEGER);
        $result = $insertQuery->execute();
    }

    // TODO: email requests

    echo $billID;
?>
