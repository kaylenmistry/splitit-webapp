<?php
    error_reporting(E_ALL);
    ini_set('display_errors','on');
    require_once('database.php');
    session_start();

    $db = new Database();

    $userID = $_SESSION["userID"];
    $billID = $_POST["billID"];
    $payment = $_POST["payment"];

    $selectQuery = $db->prepare("SELECT amountPaid, amountToPay FROM userBills WHERE userID=:userID AND billID=:billID LIMIT 1");
    $selectQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
    $selectQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
    $result = $selectQuery->execute();

    if ($row = $result->fetchArray()) {
        $amountPaid = $row['amountPaid'];
        $amountPaid += $payment;
        if ($amountPaid > $row['amountToPay']) {
            echo "please enter an amount smaller than the bill pending";
            return;
        }

        $updateQuery = $db->prepare("UPDATE userBills SET amountPaid=:amountPaid WHERE userID=:userID AND billID=:billID");
        $updateQuery->bindValue(':userID', $userID, SQLITE3_INTEGER);
        $updateQuery->bindValue(':billID', $billID, SQLITE3_INTEGER);
        $updateQuery->bindValue(':amountPaid', $amountPaid, SQLITE3_INTEGER);
        $result = $updateQuery->execute();
    }

?>